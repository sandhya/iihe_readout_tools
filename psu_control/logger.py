from zmq import DELAY_ATTACH_ON_CONNECT
from dummy_db import dummy_db
import time, os, json, sys
from zmq_interface import zmq_client

ZMQ_PORT = 5555   #Port of the ZMQ HW interface service
DELAY    = 5      #Time between measurements

db = dummy_db(file_name = "psu.db", structure = [('date', 4), ('LV1', 2), ('LV2', 2),('LV3', 2), ('LI1', 2), ('LI2', 2),('LI3', 2), ('HV',3), ('HI', 3)])


def converter(key,val):
    if "LV" in key:
        return int(1000*float(val) + 0.5)
    elif "LI" in key:
        return int(1000*float(val) + 0.5)
    elif "HV" in key:
        return int(1000*float(val) + 0.5 )
    elif "HI" in key:
        return int(1000*float(val) + 0.5 )
    else:
        return int(val)

#Connecting to hw interface service
rq = zmq_client(ZMQ_PORT)
if not rq.is_connected():
  print(f"Unable to connect to zmq server at port {ZMQ_PORT}")

i = 0
status = "running"
while True:
    t0 = time.time()
    i+=1
    print("Logging "+4*" ",end="\r")
    print("Logging "+(i%4)*".",end="\r")
    sys.stdout.flush()
    if not status == "running":
        try:
            rq.query("restart_service")
            rq = zmq_client(ZMQ_PORT)
            time.sleep(1)
        except Exception as e:
            print(e)
    try:
        res_0, res_1 = "",""
        res_0 = rq.query("LV get_measurements")
        res_1 = rq.query("HV get_measurements")
        entry = json.loads(res_0)
        entry.update(json.loads(res_1))
        if entry["HV"] > 5 or rq.query("HV get_status") == "1":
            rq.query("led set_state locked")
        else: 
            rq.query("led set_state unlocked")

        entry["date"] = int(time.time())

        for key in entry:
            val = entry[key]
            entry[key] = converter(key,val)
        db.add(entry)
        db.write_to_file()
    except Exception as e:
        print(f"Logger error : {e}")
        print(f"Results from queries : {res_0} {res_1}")
        status = "failed"
        print("Reconnecting to zmq...")

    dt = time.time()-t0
    if dt < DELAY:
      time.sleep(DELAY - dt)

