
import pyvisa
import psu_common
import time,os,json

VOLT_LIMITS = [0,900]

class DT54xx:
    def __init__(self,tty, serial_number = None):
        self.inst = psu_common.open_device(tty)
        self.tty = tty
        self.V0 = 0
        self.I0 = 0
        if self.inst == None:
            print("Fatal error, unable to connect to PSU !")

    def claimed_port(self):
        return self.tty

    def query(self,command):
        r_str=self.inst.query(command).replace("\x11","").replace("\r","").replace("\n","").replace("\x13","")
        r_str = r_str.split(",")
        if len(r_str)==0:
            return("ERR : No answer from device")
        elif len(r_str) == 1:
            if "CMD:" in r_str[0]:
                return r_str[0].split("CMD:")[1]
            else:
                return (f"ERR : return value is : {r_str}")
        elif len(r_str) == 2:
            if "CMD:OK" in r_str[0] and "VAL:" in r_str[1]:
                return r_str[1].split("VAL:")[1]
            else:
                return (f"ERR : return value is : {r_str}")



    def write(self,command):
        self.inst.write(command)
        return 1

    def get_id(self):
        return(self.query("$CMD:MON,PAR:BDNAME\r"))

    def channel_check(self,channel):
        if channel != 0:
            print(f"Warning, trying to access unexisting channel {channel}")

    def get_voltage(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:MON,PAR:VMON\r")

    def get_current(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:MON,PAR:IMON\r")

    def get_measurements(self):
        meas = {}
        meas[f"HV"] = float(self.get_voltage())
        meas[f"HI"] = float(self.get_current())
        return json.dumps(meas)


    def allow_local(self):
        return("ERR : This command isn't possible on this PSU")

    def set_local(self):
        return("ERR : This command isn't possible on this PSU")

    def is_enabled(self,channel = 0):
        return (self.get_status())%2

    def get_status_list(self,channel = 0):
        self.channel_check(channel)
        stat = self.get_status()
        stl = []
        if stat %2 == 1:
            stl.append("ON")
        else:
            stl.append("OFF")
        messages = ["ON", "RAMP UP", "RAMP DOWN", "OVER CURRENT", "OVER VOLTAGE", "UNDER VOLTAGE", "MAXV PROTECTION", "TRIP", "OVER TEMP", "res", "DISABLED", "KILL", "INTERLOCK", "CALIB ERROR"]
        for bb in range(1,14):
            if (stat >>bb)%2 ==1:
                stl.append(messages[bb])
        return stl

    
        

    def get_status(self,channel = 0):
        self.channel_check(channel)
        stat = int(self.query("$CMD:MON,PAR:STAT\r"))
        return stat


    def turn_on(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:SET,PAR:ON\r")

    def turn_off(self, channel = 0):
        self.channel_check(channel)
        return self.query("$CMD:SET,PAR:OFF\r")

    def get_v_set(self, channel = 0):
        self.channel_check(channel)
        return(float(self.query("$CMD:MON,PAR:VSET\r")))

    def set_v(self, volt, channel = 0):
        self.channel_check(channel)

        volt = float(volt)
        if volt < VOLT_LIMITS[0] or volt > VOLT_LIMITS[1]:
            raise "Voltage ({volt}) out of bounds !"
        return (self.query(f"$CMD:SET,PAR:VSET,VAL:{volt}\r"))

    def set_ramp_up(self,volt, channel = 0):
        self.channel_check(channel)
        return(self.query(f"$CMD:SET,PAR:RUP,VAL:{volt}\r"))

    def set_ramp_down(self,volt, channel = 0):
        self.channel_check(channel)
        return(self.query(f"$CMD:SET,PAR:RDW,VAL:{volt}\r"))

    def clear_error(self,channel = 0):
        self.channel_check(channel)
        return(self.query(f"$CMD:SET,PAR:BDCLR\r"))

if __name__=="__main__":
    #import zmq_interface
    #zmq_interface.zmq_server({"0":NGE100(tty="ASRL/dev/ttyACM1::INSTR")})
    #psu_common.list_devices()
    #hv_psu = DT54xx(tty="ASRL/dev/ttyACM0::INSTR")

    #print(hv_psu.get_id())
    #print(hv_psu.set_v(100))
    #print(hv_psu.get_v_set())
    #print(hv_psu.get_voltage())
    #print(hv_psu.get_measurements())
    #print(hv_psu.turn_on())
    #print(hv_psu.turn_off())
    #psu_common.list_devices()
    #hv_psu = DT54xx(tty="ASRL/dev/ttyACM0::INSTR")
    #print(hv_psu.set_ramp_down(5))
    #print(hv_psu.set_ramp_up(5))
    #print(psu_common.get_tty("DT54XX USB HV POWER SUPPLY"))
    hv_psu = DT54xx(tty=f"ASRL{psu_common.get_tty('DT54XX USB HV POWER SUPPLY')[0]}::INSTR")
    hv_psu.clear_error()
    
    """print(hv_psu.query("$CMD:MON,PAR:ISET\r"))
    print(hv_psu.query("$CMD:SET,PAR:ISET,VAL:50.0\r"))
    
    print(hv_psu.set_v(5))
    hv_psu.turn_on()
    time.sleep(1)
    hv_psu.turn_off()
    print(hv_psu.query("$CMD:MON,PAR:ISET\r"))
    print(hv_psu.query("$CMD:SET,PAR:IMRANGE,VAL:LOW\r"))
    #print(hv_psu.query("$CMD:MON,PAR:IMRANGE\r"))"""


