import zmq_interface
import psu_common
from Dummy_psu import Dummy_psu
from DT54xx import DT54xx
from NGE100 import NGE100
from led_strip import led_strip
import time
#zmq_interface.zmq_server({"0":NGE100(tty="ASRL/dev/ttyACM1::INSTR")})

print("Starting up...")
print("List of devices:")
psu_common.print_devices()

#print(psu_common.list_devices())

#0 = LV
#1 = HV
rc = 1
while rc == 1:
    try:
        devs = {"LV" : Dummy_psu(), "LV" : Dummy_psu(), "led" : led_strip()}        
        devs["led"].run()
        lv_ports = psu_common.get_tty("ROHDE & SCHWARZ Device - CDC Abstract Control Model (ACM)")
        if len(lv_ports) >= 1:
            if len(lv_ports) >1:
                print("Warning, found multiple LV PSU!")
            try:
                print(lv_ports[0])
                devs["LV"] = NGE100(tty=f"ASRL{lv_ports[0]}::INSTR")
            except Exception as e:
                print(f"FATAL ERROR : {e}")
                devs["LV"] = Dummy_psu()
        else:
            print("Warning, LV PSU not found...")

        hv_ports = psu_common.get_tty("DT54XX USB HV POWER SUPPLY")
        if len(hv_ports) >= 1:
            if len(hv_ports) >1:
                print("Warning, found multiple HV PSU!")
            try:
                devs["HV"] = DT54xx(tty=f"ASRL{hv_ports[0]}::INSTR")
            except Exception as e:
                print(f"FATAL ERROR : {e}")
                devs["HV"] = Dummy_psu()
        else:
            print("Warning, HV PSU not found...")
        
        zs = zmq_interface.zmq_server(devs)
        rc = zs.run()
        time.sleep(1)
    except Exception as e:
        print(e)
