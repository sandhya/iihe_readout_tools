from zmq_interface import zmq_server, zmq_client
from blinkstick import blinkstick
import time
import contextlib
import threading

class led_strip:
    def __init__(self):
        self.connect()
        self.anim_index = 0

        self.thread = None
        self.thread_running = False
        self.prev_state = None
        self.state = "booting"
        self.last_update = 0

    def connect(self):
        stick_list = blinkstick.find_all()
        if len(stick_list) > 0:
            self.stick = stick_list[0]
            self.stick.set_error_reporting(True)
            return True
        else:
            self.stick = None
            return False

    def check_connection(self):
        if self.stick == None:
            if not self.connect():
                print("Error, no blink stick found")
                time.sleep(5)
                return 0
        return 1


    def runner(self,func,args):
        if not self.check_connection():
            return 0
        success = False
        n_tries = 0
        while not success and n_tries < 100:
            rc = 0
            try:
                # with contextlib.redirect_stdout(None):
                func(**args)
                time.sleep(0.001)
                success = True
            except Exception as e:
                n_tries+=1
        if not success:
            print(f"Error, unable to connect after {n_tries} tries.")
            self.stick = None


    def booting(self):
        if not self.check_connection():
            return 0

        if (self.anim_index%32) == 0:
            self.turn_off()
        time.sleep(0.01)
        light_data = [0 for i in range(3*32)]
        ri = self.anim_index%32
        for i in range(ri):
            light_data[3*i+2] = 255
        # self.runner(self.stick.set_color,{"blue":255,"index":self.anim_index%32})
        self.runner(self.stick.set_led_data,{"channel":0,"data":light_data})
        self.anim_index += 1

    def locked(self):
        if not self.check_connection():
            return 0

        self.anim_index += 1
        ri = self.anim_index%60
        pol = int(self.anim_index/60)%2
        light_data = [0 for i in range(3*32)]

        for i in range(32):
            fc = 0
            if ri < 40:
                fc = 200*((i%2)==pol)
            else:
                rri = ri-40
                if (i%2) == pol:
                    fc = 200-10*rri
                else:
                    fc = 10*rri
            light_data[3*i+1] = fc

        self.runner(self.stick.set_led_data,{"channel":0,"data":light_data})

    def unlocked(self):
        if not self.check_connection():
            return 0

        self.anim_index += 1
        first_led = self.anim_index%52-20

        light_data = [0 for i in range(3*32)]

        # for sl in range(16):
        #     intensity = (1<<(int(sl/2)))-1
        #     led = first_led+sl
        #     if led >= 0 and led < 32:
        #         light_data[3*led] = intensity

        for sl in range(20):
            intensity = 10*sl
            led = first_led+sl
            if led >= 0 and led < 32:
                light_data[3*led] = intensity

        self.runner(self.stick.set_led_data,{"channel":0,"data":light_data})


    def disconnected(self):
        if not self.check_connection():
            return 0

        self.anim_index += 1
        light_data = [0 for i in range(3*32)]

        ri = self.anim_index%40
        pol = int(self.anim_index/40)%2
        fc = 0
        if ri < 20:
            fc = 200*pol
        else:
            rri = ri-20
            if pol:
                fc = 200-10*rri
            else:
                fc = 10*rri
        for led in range(32):
            light_data[3*led+1]  =int(fc/255.*0xAA)
            light_data[3*led+0]  =int(fc/255.*0x3B)
            light_data[3*led+2]  =int(fc/255.*0x00)
            # self.runner(self.stick.set_color,{"red":0xDE*(self.anim_index%2),"green":0x53*(self.anim_index%2),"blue":0x07*(self.anim_index%2),"index":led})
        # self.runner(self.stick.set_led_data,{"channel":0,"data":light_data})
        # print(light_data)

        self.runner(self.stick.set_led_data,{"channel":0,"data":light_data})

    def turn_off(self):
        if not self.check_connection():
            return 0

        light_data = [0 for i in range(3*32)]
        self.runner(self.stick.set_led_data,{"channel":0,"data":light_data})

        # for led in range(32):
        #     self.runner(self.stick.set_color,{"index":led})

    def set_state(self,state):
        self.state = state
        self.last_update = time.time()

    def run_thread(self):
        self.thread_running = True
        while self.state != "exit":
            self.thread_running = True
            
                
            if self.state != "booting" and time.time() - self.last_update > 10:
                self.state = "disconnected"

            if self.state != self.prev_state:
                self.anim_index = 0
                self.turn_off()
                self.prev_state = self.state
            
            elif self.state == "locked":
                self.locked()
                time.sleep(0.05)
            elif self.state == "unlocked":
                self.unlocked()
                time.sleep(0.05)
            elif self.state == "exit":
                self.turn_off()
            elif self.state == "booting":
                self.booting()
                time.sleep(0.05)
            else:
                self.disconnected()
                time.sleep(0.05)
        self.thread_running = False

            
    def run(self):
        print("Starting blinkstick runner...", end = "")
        if self.thread == None or self.thread_running == False:
            self.thread = threading.Thread(target=self.run_thread)
            self.thread.start()
        print(" Done !")

if __name__ == "__main__":
    # def my_func():
    #     ls = led_strip()
    #     rq = zmq_server({"led":ls})
    #     ls.run()
    #     rq.run()
    # server_thread = threading.Thread(target=my_func)
    # server_thread.start()
    # print("Server started")
    # # time.sleep(1000)
    # client = zmq_client()
    # # print(client.query("led run"))
    ls = led_strip()
    rq = zmq_server({"led":ls})
    ls.run()
    rq.run()
