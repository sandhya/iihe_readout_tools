#!/bin/bash

tmux new-session -d -n "psu_hw_interface"
tmux send-keys -t "psu_hw_interface" 'cd $IIHE_TOOLS_BASE_DIR/psu_control; python3 psu_interface.py' C-m

tmux new-session -d -n "psu_logger"
tmux send-keys -t "psu_logger" ' cd $IIHE_TOOLS_BASE_DIR/psu_control; python3 logger.py' C-m

tmux new-session -d -n "web_server"
tmux send-keys -t "web_server" '$IIHE_TOOLS_BASE_DIR/web_interface/runner.sh' C-m

