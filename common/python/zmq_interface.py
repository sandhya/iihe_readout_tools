import zmq, time

class zmq_server:
  def __init__(self,target_objects, port = 5555):
    print(f"Launching zmq_server on port {port}")
    self.obj  = target_objects
    self.port = port
  
  def run(self):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind(f"tcp://*:{self.port}")
    while True:
      message = socket.recv().decode("utf-8")
      print("Received request: %s" % message)
      if message == "restart_service":
        socket.send(bytes("restarting","utf-8"))
        return 1
      return_message = self.process_message(message)
      socket.send(bytes(return_message,"utf-8"))

  def process_message(self,message):
    try:
      if message.lower() == "ping":
          return("pong")
      elif message.lower() == "list_objects":
          return(f"{self.obj}")
      elif message.split(" ")[0] in self.obj.keys():
          message = message.split(" ")
          target_obj = self.obj[message[0]]
          if len(message) == 1:
            return("pong")
          elif message[1] in target_obj.__class__.__dict__.keys():
            if len(message) == 2:
              return str(target_obj.__class__.__dict__[message[1]](target_obj))
            else:
              return str(target_obj.__class__.__dict__[message[1]](target_obj,*message[2:]))
          else:
            return(f"error, {message[1]} not a method of {message[0]}")
      else: 
        return(f"error : {message.split(' ')[0]} not a recognized object label")
    except Exception as e:
      return(f"error : {e}")


class zmq_client:
    def __init__(self, port = 5555, verbose = False):
      context = zmq.Context()
      self.stored_context = context
      context.RCVTIMEO = 1000
      if verbose:
        print(f"Connecting to zmq server at port {port}…")
      self.socket = context.socket(zmq.REQ)
      self.socket.connect(f"tcp://localhost:{port}")
      self.socket.setsockopt (zmq.LINGER, 0 )
      self.connected = False
      try:
        self.socket.send(bytes("ping",'utf-8'))
      except Exception as e:
        if verbose:
          print(f"Unable to send ping command... {e}")
        self.socket = None
        return None
      try:        
        answer = self.socket.recv()
        self.connected = (answer.decode("utf-8") == "pong")
      except Exception as e:
        if verbose:
          print(f"Unable to connect : {e}")
        self.socket = None

      if not self.connected:
        print(f"Error, unable to connect to port {port}")

    def is_connected(self):
      return self.connected

    def query(self,message):
        if self.socket == None:
          print("Error, socket not available")
          return None
        self.socket.send(bytes(message,'utf-8'))
        return self.socket.recv().decode("utf-8")

if __name__ == "__main__":
  from random import random
  class dummy_psu:
    def __init__(self, id):
      self.id = id
    def ping(self, msg = ""):
      return (f"pong {msg}")
    def two_args (self, arg1, arg2):
      return (f"{arg1} {arg2}")
    def get_voltage(self, channel):
      channel = int(channel)
      return 0.1*random() + channel

    def get_current(self, channel):
      channel = int(channel)
      return 10*random() + 100*channel

  #psu_list = {"0" : dummy_psu(0)}
  #zmq_server(psu_list)
  """
  context = zmq.Context()
  socket = context.socket(zmq.REQ)
  socket.connect(f"tcp://localhost:{5555}")
  socket.send(bytes("ping",'utf-8'))
  timeout = 5
  start = time.time()
  connected = False
  while not connected and time.time() - start < timeout:
    try:
      socket.recv(zmq.NOBLOCK)
      print(f"Connected after {time.time()-start} seconds")
      connected = True
    except Exception as e:
      time.sleep(0.01)
  
    
  #time.sleep(0.1)
  """
  cc = zmq_client(5555, True)
  print("----")
  cc = zmq_client(5554, True)
  print("----")
  cc = zmq_client(5553, True)
  print("----")
  
