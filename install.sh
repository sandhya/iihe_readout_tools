#!/bin/bash
source setup.sh

bashrc_content=$(cat ~/.bashrc)
startup="source $IIHE_TOOLS_BASE_DIR/setup.sh"

if [[ "$bashrc_content" == *"$startup"* ]];
then
  echo "Script already sourced in bashrc"
else
  echo "Adding setup.sh in bashrc..."
  echo $startup >> ~/.bashrc
fi

bin_dir=$IIHE_TOOLS_BASE_DIR/bin
if [ ! -d "$bin_dir" ]; then
  # Take action if $DIR exists. #
  echo "Creating bin directory..."
  mkdir $bin_dir
fi

bin_list="plotting_scripts/plot_2S_trend.py plotting_scripts/plot_2S_noise.py psu_control/psu_control plotting_scripts/process_last_run.py"
for f in $bin_list ; do 
    if [ ! -f "$bin_dir/$(basename $f)" ]; 
    then
        echo "Linking script $(basename $f)"
        ln -s $IIHE_TOOLS_BASE_DIR/$f $bin_dir/
    else
        echo "Script $f already linked"
    fi

done
