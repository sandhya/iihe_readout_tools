import os, glob

#Split by voltage :
def split_by_voltage(base_dir, args):
    dir_by_voltage = {}
    index = 0
    for base_name in sorted(glob.glob(base_dir+"/*"),key=os.path.getmtime):
        print(base_name)
        base_name = base_name.split("/")[-1]
        index +=1
        voltage = None
        has_prefix = False
        for name_comp in base_name.split("_"):
            if "V" in name_comp and name_comp.replace("V","").isdigit():
                voltage = int(name_comp.replace("V",""))
            if args.prefix and args.prefix in name_comp:
                has_prefix = True
        if voltage == None:
            print(f"Error, couldn't get voltage of folder {base_name}")
            continue
        if args.prefix and not has_prefix:
            print(f"Skipping {base_name} (prefix not present)")
            continue

        if voltage in dir_by_voltage.keys():
            dir_by_voltage[voltage].append(base_name)
        else:
            dir_by_voltage[voltage] = [base_name]

    return dir_by_voltage

def split_by_date(base_dir, args):
    dir_by_date = {}
    index = 0
    for base_name in sorted(glob.glob(base_dir+"/*"),key=os.path.getmtime):
        print(base_name)
        base_name = base_name.split("/")[-1]
        index +=1
        date = base_name.split("_")[-1]
        has_prefix = False
        for name_comp in base_name.split("_"):
        #    date = name_comp
        #    print(date)
        #    if "V" in name_comp and name_comp.replace("V","").isdigit():
        #        date = int(name_comp.replace("V",""))
            if args.prefix and args.prefix in name_comp:
                has_prefix = True
        if date == None:
            print(f"Error, couldn't get date of folder {base_name}")
            continue
        if args.prefix and not has_prefix:
            print(f"Skipping {base_name} (prefix not present)")
            continue
        #print(dir_by_date)
        if date in dir_by_date.keys():
            dir_by_date[date].append(base_name)
        else:
            dir_by_date[date] = [base_name]
        #print(dir_by_date)
    return dir_by_date


def get_hist_data(hh):
    return [hh.GetBinContent(i+1) for i in range(hh.GetNbinsX())]

def data_filler(data,value,*keys):
    sub_data = data
    for pos,kk in enumerate(keys):
        if not kk in sub_data.keys():
            if pos == len(keys) - 1:
                sub_data[kk] = []
            else:
                sub_data[kk]={}
        sub_data = sub_data[kk]
        if pos == len(keys) - 1:
            sub_data.append(value)
